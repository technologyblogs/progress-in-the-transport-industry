With rapid development in the areas of technology, people have begun to wonder when the transport industry will begin to take a revolutionizing turn. Innovation has led us to some remarkable notions such as big data, internet of things, machine learning, and artificial intelligence. When these ideas begin to find a common ground, we see concepts such as autonomous trucks and self-driving cars on the road. 
It is high time that transport production companies begin to re-adjust their business models and embrace the new concepts in transport industry. Questions arise as to the trust, confidence, and safety of a self-driving vehicle. The progress in the industry surely has brought about some phenomenal changes. Along with the new ideas, there comes a revolution in the transport industry as people begin to wonder, where do we go from here?
While the abovementioned technologies have emerged into the market, they are only being prototyped for the production of autonomous vehicles. As of now, nothing solid is in place that could say that the transport industry has drastically changed. However, here are some of the developments that are in line.

**Self-Driving Cars**

Predictions estimate that by the year 2020, there will be around 250 million smart cars and around 10 million self-driving cars on the roads. Different companies have already tested and launched smart cars, which include Tesla, BMW and Mercedes. Google is also deeply embedded in the race for self-driving cars and has launched its own project titled Waymo. With constant developments in the areas of machine learning and internet of things, it seems like self-driving cars will be normalized in no time.

**Autonomous Trucks**

Trucks are an essential part of the tertiary sector of every country. For this very reason, the idea of autonomous trucks is coming about. Daimler Trucks launched a completely autonomous truck which has the ability to initiate an auto-pilot system when necessary. Einride, a startup in Sweden, developed a self-driving truck that is not only autonomous but also remote controlled. 

**Aviation**

While there are developments being done on the ground, transport in the air is also being looked at. Amazon has developed a project where it aims to use drones to deliver its products. Similarly, Dubai just tested an unmanned two-seater air taxi for tourism purposes.

**Cargo Ships**

Remote-controlled cargo ships are also being developed to transport goods without the need of human intervention. These would be controlled from land and will have plenty of room to store goods and supplies. Rolls Royce is developing this model and aims to launch it by the year 2020.

**Machine Learning**

With the use of new techniques in technology, transport companies are using the data to further improve their solutions. Machine learning and predictive analysis allow for the optimization of processes such as routing and tracking. People begin to wonder whether services, such as [courier in Aylesbury](https://silverbullet-express.co.uk/services/aylesbury/courier-aylesbury/), will eventually move toward a more progressive transport industry with the use of technological developments.
